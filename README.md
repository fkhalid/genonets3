# Genonets

Python 3 port of the Genonets Python package.

## Updates

* **v0.32** The following changes were made:
  1. Bug fix: The function used to check the mutation type has been fixed so that it also considers 
     reverse complements when required.
  2. In certain cases when multiple genotype sets are being processed, for one or a few of them, 
     a certain analysis might not be run due to a certain condition not being met, e.g., the network 
     may be too small. In such cases, the corresponding output attribute is now set to `None`. Previously, 
     this would generate an error.
  3. Further automated tests have been added for DNA, RNA, Protein, and Binary alphabet types that test 
     all analysis functions.
  4. The `genonets_` prefix has been removed from the names of all sub-modules.
* **v0.31** Automated tests have been added for peaks, paths, epistasis, and interface edges. Also, 
  result file for each genotype set is generated only if there are results to report.
* **v0.30** A new boolean command-line parameter `--use-all-components` (and a corresponding config 
  parameter `use_all_components`) has been added, which when provided on the command-line (or set 
  in the config), enables the evolvability analysis to consider all connected components rather 
  than just the giant component (i.e., the dominant network). The `--use-all-components` parameter 
  currently only applies to the evolvability analysis. This parameter is disabled by default, 
  i.e., only the dominant network is considered unless this parameter is explicitly set.
* **v0.29** Performance of paths analysis has been further improved for very large networks. See 
  [issue 14](https://gitlab.com/fkhalid/genonets3/-/issues/14) for further details.
* **v0.28** Paths analysis now supports handling of mulitple summits as required in 
  [issue 10](https://gitlab.com/fkhalid/genonets3/-/issues/10).
* **v0.27** Bug fix for [issue 11](https://gitlab.com/fkhalid/genonets3/-/issues/11): If 
  a genetic code file is provided, the alphabet is taken from that file, instead 
  of the standard protein alphabet.
* **v0.26** Integrated the very efficient BFS based paths analysis implementation.
* **v0.25** Evolvability analysis now generates a new output parameter in the 
  `Genotype_set_parameters` file called `Interface_edges`. This parameter is a dictionary 
  in which keys are the names of genotype sets to which the focal genotype set can evolve, and 
  values are further dictionaries containing counts of each type of mutation that can lead to 
  evolution.
* **v0.24** A new command-line argument and configuration parameter `store-epistasis-squares` has 
  been added. If this argument is enabled and epistasis analysis is used, all squares are written 
  to disk. Four files are generated, where each file corresponds to one of the epistasis types, 
  including 'no epistasis'. Once the analysis is complete, these files are compressed into a zip 
  archive called `epistasis_squares.zip`. The archive is available in the output directory.
* **v0.23:** Rules that determine which type of epistasis is represented in a 
  square have been changed. See details [here](https://gitlab.com/fkhalid/genonets3/-/issues/6).
* **v0.22:** The following changes have been made:
    1. Epistasis analysis has been optimized for performance.
    2. Also, when a genetic code is in use, the epistasis analysis now considers only those 
       squares where the focal genotype and the last genotype in the square are not 1-neighbors 
       even if the genetic code were not in use.
    3. Epistasis analysis no longer support sampling. As a result the command-line argument 
       `--epistasis-sample` is no longer available.
* **v0.21:** Peaks analysis, as well as input file reading are much faster. 

## Installation

Please note that only Python 3.8 and above are supported.

Before installing Genonets, please use the following command to upgrade `pip` and `setuptools`:

```
pip install -U pip setuptools
```

Then install Genonets using the following command:

```
python -m pip install git+https://gitlab.com/fkhalid/genonets3.git
```

## What is different in this `Genetic_code` branch?

1. The new implementation of Peaks analysis has been ported from v1.1.9 of Genonets 
   for Python 2.
2. Supports processing of non-standard genetic codes.
3. Command-line arguments are supported in a different format.
4. A configuration file can be used instead of specifying command-line arguments.
5. Names of the sample files have been simplified.
6. The peaks analysis does not calculate "Distance to summit". A separate analysis type `DISTANCE_TO_SUMMIT` has to 
   requested explicitly.

## Known issues

1. The results of `DISTANCE_TO_SUMMIT` do not match those from Genonets for Python 2. This needs to be investigated.

## Updated command-line format

Following is a sample command,

```
python -u -m genonets.sample.minimal --alphabet=DNA --include-indels=True --input-file=input.csv --tau=0.35 --output-path=results --verbose
```

Positional arguments are no longer supported. Each argument has to be explicitly specified by 
using the argument name. The following command can be used to list all supported command-line 
arguments:

```
python -u -m genonets.sample.minimal -h
```

## The configuration file

In order to simplify the command-line, all command-line arguments can be loaded from a configuration file. E.g.,

```
python -u -m genonets.sample.minimal -c config.cfg
```

The above command will load all command-line arguments from `config.cfg`.

Following is a sample configuration file:

```
[General]
input_file: total_dataset.csv
output_path: results
tau: 3.0
verbose: yes
alphabet: Protein
include_indels: yes
num_processes: 1
use_reverse_complements: no
store_epistasis_squares: no
use_all_components: no

[Genetic Code]
codon_alphabet: RNA
include_indels_for_codons: yes
use_reverse_complements_for_codons: no
genetic_code_file: GC_S.csv
```

The `Genetic Code` section is optional.

**Note:** If both a configuration file and command-line arguments are used, the command-line 
arguments override values in the configuration file.
